﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace EquineSystem.App_Start
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            //bundle for login page
            bundles.Add(new StyleBundle("~/Content").Include(
                "~/Content/assets/plugins/bootstrap/css/bootstrap.min.css",
                //"~/Content/assets/plugins/bootstrap/css/bootstrap.min.css",
                "~/Content/css/style.css",
                "~/Content/css/colors/megna-dark.css",
                "~/Content/assets/plugins/calendar/dist/fullcalendar.css",
                "~/Content/assets/plugins/sweetalert/sweetalert.css"
               ));

            //bundle for dashboard template
            //bundles.Add(new StyleBundle("~/Content/assets").Include(
            //    "~/Content/assets/plugins/bootstrap/css/bootstrap.min.css",
            //    "~/Content/css/style.css",
            //    "~/Content/css/colors/green-dark.css"
            //   ));


        }
    }
}