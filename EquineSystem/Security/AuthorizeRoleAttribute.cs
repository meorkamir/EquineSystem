﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Principal;
using EquineSystem.Models.EntityManager;
using EquineSystem.Models;
using System.Web.Mvc;

namespace EquineSystem.Security
{
    public class AuthorizeRoleAttribute: AuthorizeAttribute
    {
        private readonly string[] userAssignedRoles;

        //constructor
        public AuthorizeRoleAttribute(params string[] roles)
        {
            this.userAssignedRoles = roles;
        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            bool authorize = false;
            using (EquineDBEntities db = new EquineDBEntities())
            {
                UserManager UM = new UserManager();
                foreach (var roles in userAssignedRoles)
                {
                    authorize = UM.UserInRole(httpContext.User.Identity.Name,roles);
                    if (authorize)
                        return authorize;
                }
            }
            return authorize;
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.Result = new RedirectResult("~/Home/Unauthorized");
        }

    }

    

}
