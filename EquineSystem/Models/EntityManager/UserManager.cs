﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EquineSystem.Models.EntityManager;

namespace EquineSystem.Models.EntityManager
{
    public class UserManager
    {
        //method to check users role
        public bool UserInRole(string emailId, string roleName)
        {
            using(EquineDBEntities dc=new EquineDBEntities())
            {
                //check whether user exist
                User user = dc.Users.Where(o => o.EmaidId.Equals(emailId))?.FirstOrDefault();
                if(user != null)
                {
                    var role = from q in dc.Users
                               where q.EmaidId.Equals(emailId) && q.Role.Equals(roleName)
                               select q.Role;

                    if (role != null)
                        return role.Any();
                }

                return false;
            }
        }
    }
}