﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace EquineSystem.Models
{
    [MetadataType(typeof(UserMetadata))]  //appy validation to main class
    public partial class User
    {
        public string ConfirmPassword { set; get; }
    }

    //create field validation
    public class UserMetadata
    {
        [Display(Name ="First Name")]
        [Required(AllowEmptyStrings =false, ErrorMessage ="First name required")]
        public string FirstName { set; get; }

        [Display(Name = "Last Name")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Last name required")]
        public string LastName { set; get; }

        [Display(Name = "Email Address")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Email required")]
        [DataType(DataType.EmailAddress)]
        public string EmaidId { set; get; }

        [Display(Name = "Date of Birth")]
        //[Required(AllowEmptyStrings = false, ErrorMessage = "Email required")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode =true, DataFormatString ="{0:MM/dd/yyy}y")]
        public string DateOfBirth { set; get; }

        [Display(Name = "Password")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Password is required")]
        [DataType(DataType.Password)]
        [MinLength(6,ErrorMessage ="Minimum 6 character required")]
        public string Password { set; get; }

        //[Display(Name ="Confirm Password")]
        //[Required(AllowEmptyStrings = false, ErrorMessage = "Password is required")]
        //[DataType(DataType.Password)]
        //[Compare("Password", ErrorMessage ="Confirm password and password mismatch")]
        public string ConfirmPassword { set; get; }

    }
}