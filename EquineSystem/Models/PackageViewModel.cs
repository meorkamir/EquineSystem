﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EquineSystem.Models
{
    public class PackageViewModel
    {
        public int PackageId { get; set; }
        public string PackageName { get; set; }
        public string Description { get; set; }
        public string IMG { get; set; }
        public double Price { get; set; }
        public int Status { get; set; }
    }
}