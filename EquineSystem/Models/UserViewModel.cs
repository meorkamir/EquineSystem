﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EquineSystem.Models
{
    public class UserViewModel
    {
        public int UserId { get; set; }
        public string FirstName { set; get; }
        public string LastName { set; get; }
        public string EmaidId { set; get; }
        public string Role { set; get; }
        public string Status { set; get; }
        public string Password { set; get; }
    }
}