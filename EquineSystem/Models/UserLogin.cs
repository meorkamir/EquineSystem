﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace EquineSystem.Models
{
    public class UserLogin
    {
        [Display(Name ="Email Id")]
        [Required(AllowEmptyStrings =false,ErrorMessage ="Email is required")]
        public string EmailID { get; set; }

        [Required(AllowEmptyStrings =false,ErrorMessage ="Password is required")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Display(Name ="Remember me")]
        public bool RememberMe { get; set; }
    }
}