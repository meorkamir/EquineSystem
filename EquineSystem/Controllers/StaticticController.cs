﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EquineSystem.Models;

namespace EquineSystem.Controllers
{
    public class StaticticController : Controller
    {
        // GET: Statictic
        public ActionResult Index()
        {
            TempData["headline"] = "Statistic";
            return View();
        }

        //return statistic record -> booking
        public JsonResult PackageStat()
        {
            using (EquineDBEntities dc = new EquineDBEntities())
            {
                var events = (from b in dc.Booking
                              join p in dc.Package
                              on b.PackageId equals p.PackageId
                              group p by p.PackageName into packCat
                              select new
                              {
                                  PackageName= packCat.Key ,
                                  Quantity= packCat.Count()

                              }).ToList();

                //var events = dc.Events.ToList();
                return new JsonResult { Data = events, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
        }

        //return statistic record -> stff


    }
}