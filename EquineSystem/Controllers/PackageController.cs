﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using EquineSystem.Security;
using System.Web.Mvc;
using EquineSystem.Models;
using Newtonsoft.Json;

namespace EquineSystem.Controllers
{
    public class PackageController : Controller
    {
        EquineDBEntities dc = new EquineDBEntities();
        // GET: Package
        [AuthorizeRole("User")]
        public ActionResult Index()
        {
            TempData["headline"] = "Available Package";
            return View();
        }

        //list of package for admin role to manage
        [HttpGet]
        public ActionResult PackageList()
        {
            TempData["headline"] = "Package Management";
            return View();
        }

        public JsonResult GetPackageById(int packageId)
        {
            Package package = dc.Package.Where(x => x.PackageId == packageId).SingleOrDefault();
           // package.IMG = HttpContext.Server.MapPath(package.IMG);
            string value = string.Empty;
            value = JsonConvert.SerializeObject(package, Formatting.Indented, new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });
            return Json(value, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetPackageList()
        {
            
            List<PackageViewModel> packageList = dc.Package.Select(x => new PackageViewModel
            {
                PackageId = x.PackageId,
                PackageName = x.PackageName,
                Description = x.Description,
                Price = x.Price,
                Status = x.Status

            }).ToList();

            return Json(packageList, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult TongglePackageStatus(int id)
        {
            var status = false;
                    var v = dc.Package.Where(a => a.PackageId == id).FirstOrDefault();
                    if (v != null)
                    {
                        if (v.Status == 1)
                        {
                            v.Status = 2;
                        }
                        else {
                            v.Status = 1;
                        }

                        status = true;
                    }
               
                dc.SaveChanges();
               
            return new JsonResult { Data = new { status = status } };
        }

        [HttpPost]
        public ActionResult AddNewPackageWithImg(int PackageId, string PackageName,string Description, float Price)
        {
            var status = false;
            var fileStatus = false;
            string absolutePath="";
            HttpPostedFileBase file = Request.Files["ImageFile"];
            //check file status
            if (file.ContentLength>0)
            {
                fileStatus = true;
            }
            

            try
            {
                if (fileStatus)//save file if file not null
                {
                    string fileName = Path.GetFileNameWithoutExtension(file.FileName);
                    string extension = Path.GetExtension(file.FileName);
                    fileName = fileName + DateTime.Now.ToString("yymmssfff") + extension;
                    absolutePath = "~/Gallery/" + fileName;
                    fileName = Path.Combine(Server.MapPath("~/Gallery/"), fileName); //customized filename
                                                                                     //save file to server folder
                    file.SaveAs(fileName);
                }
                else//set file to default image
                {
                    absolutePath = "~/Content/assets/images/big/img6.jpg";
                }


                //create object
                Package package = new Package();
                package.IMG = absolutePath;
                package.PackageName = PackageName;
                package.Description = Description;
                package.Price = Price;
                package.Status = 1;

                using (EquineDBEntities dc = new EquineDBEntities())
                {
                    if (PackageId == 0)//new package creation
                    {
                     //add new package
                      dc.Package.Add(package);
                 
                    }
                    else
                    {
                        //update existing package
                        var v = dc.Package.Where(a => a.PackageId == PackageId).FirstOrDefault();
                        if (v != null)
                        {
                            v.PackageName = PackageName;
                            v.Price = Price;
                            v.Description =Description;

                            //admin update package wihout updating image
                            if (absolutePath == "")
                            {
                                v.IMG = absolutePath;
                            }
                            
 
                        }
                    }
                    dc.SaveChanges();
                    status = true;
                }
            }
            catch (Exception e)
            {
                //exception handling
                string error = e.ToString();
            }

            string statusResult;
            if (status)
            {
                statusResult = "Success";
            }
            else
            {
                statusResult = "Failed";
            }

           // return RedirectToAction("PackageList");
            return Content("<script language='javascript' type='text/javascript'>alert('"+ statusResult + "'); window.location='/Package/PackageList'</script>");

        }

        //delete package
        //[AuthorizeRole("Admin")]
        [HttpPost]
        public ActionResult DeletePackage(int packageId)
        {
            bool status = false;

            using (EquineDBEntities dc = new EquineDBEntities())
            {
                var v = dc.Package.Where(a => a.PackageId == packageId).FirstOrDefault();
                if (v != null)
                {
                    dc.Package.Remove(v);
                    dc.SaveChanges();
                    status = true;
                }
            }
            return new JsonResult
            {
                Data = new { status = status }
            };
        }

        //place booking
        public JsonResult PlaceBooking(Booking booking, string userId)
        {
            //recognize user credential
            int userID;
            using (EquineDBEntities dc = new EquineDBEntities())
            {
                var user = dc.Users.Where(a => a.EmaidId == userId).FirstOrDefault();
                userID = user.UserId;
            }
            
            //Booking 
            booking.UserId = userID;
            booking.TimeStamp = DateTime.Now;

            var status = false;
            using (EquineDBEntities dc = new EquineDBEntities())
            {
               
                //add new event
                dc.Booking.Add(booking);
                
                dc.SaveChanges();
                status = true;
            }
            return new JsonResult { Data = new { status = status } };
        }

    }
}