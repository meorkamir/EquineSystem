﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EquineSystem.Models;
using EquineSystem.Security;
using Newtonsoft.Json;
using System.Net.Mail;
using System.Net;

namespace EquineSystem.Controllers
{
    public class AdminController : Controller
    {
        EquineDBEntities dc = new EquineDBEntities();
        // GET: Admin
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Calender()
        {
            TempData["headline"] = "Calender";
            return View();
        }

        //get events for calender --> json
        public JsonResult GetEvent()
        {

            using (EquineDBEntities dc = new EquineDBEntities())
            {
                var events = (from e in dc.Events
                              join u in dc.Users
                              on e.UserId equals u.UserId
                              select new
                              {
                                  Writer = u.FirstName + " " + u.LastName,
                                  Subject = e.Subject,
                                  Description = e.Description,
                                  Start = e.Start,
                                  End = e.End,
                                  ThemeColor = e.ThemeColor,
                                  IsFullDay = e.IsFullDay

                              }).ToList();

                //var events = dc.Events.ToList();
                return new JsonResult { Data = events, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
        }

        [HttpGet]//baru tambah
        public JsonResult getCalender()
        {
            using (EquineDBEntities dc = new EquineDBEntities())
            {


                var calender = (from e in dc.Events
                                join u in dc.Users
                                on e.UserId equals u.UserId
                                select new
                                {
                                    EventId=e.EventId,
                                    Origin = "Event",
                                    Writer = u.FirstName + " " + u.LastName,
                                    Subject = e.Subject,
                                    Description = e.Description,
                                    Start = e.Start,
                                    End = e.End,
                                    ThemeColor = e.ThemeColor
                                    // IsFullDay = e.IsFullDay

                                }).Union(from b in dc.Booking
                                         join u in dc.Users
                                         on b.UserId equals u.UserId
                                         join p in dc.Package
                                         on b.PackageId equals p.PackageId
                                         where b.Status !=2
                                         select new
                                         {
                                             EventId=b.BookingId,
                                             Origin = "Booking",
                                             Writer = u.FirstName + " " + u.LastName,
                                             Subject = "Booking",
                                             Description = p.PackageName + "X" + b.Quantity + b.Status,
                                             Start = b.BookingDate,
                                             End = b.BookingDate,
                                             ThemeColor = "blue"
                                             //IsFullDay = null;

                                         }).ToList();

                //var events = dc.Events.ToList();
                return new JsonResult { Data = calender, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
        }

        //add new/ update event calender
        [HttpPost]
        public JsonResult SaveEvent(Events e, string id)
        {
            //recognized user id
            int userId;
            using (EquineDBEntities dc = new EquineDBEntities())
            {
                var user = dc.Users.Where(a => a.EmaidId == id).FirstOrDefault();
                userId = user.UserId;
            }

            ////assign writer to event
            e.UserId = userId;


            var status = false;
            using (EquineDBEntities dc = new EquineDBEntities())
            {
                if (e.EventId > 0)//existing event
                {
                    var v = dc.Events.Where(a => a.EventId == e.EventId).FirstOrDefault();
                    if (v != null)
                    {
                        v.Start = e.Start;
                        v.End = e.End;
                        v.Description = e.Description;
                        v.Subject = e.Subject;
                        v.ThemeColor = e.ThemeColor;
                        v.SpecDate = e.SpecDate;
                        //missing chekbox
                        //v.IsFullDay = e.IsFullDay;

                    }
                }
                else
                {
                    //add new event
                    dc.Events.Add(e);
                }
                dc.SaveChanges();
                status = true;
            }
            return new JsonResult { Data = new { status = status } };
        }

        //remove events
        [HttpPost]
        public JsonResult DeleteEvent(int eventId)
        {
            var status = false;
            using (EquineDBEntities dc = new EquineDBEntities())
            {
                var v = dc.Events.Where(a => a.EventId == eventId).FirstOrDefault();
                if (v != null)
                {
                    dc.Events.Remove(v);
                    dc.SaveChanges();
                    status = true;

                }
            }
            return new JsonResult { Data = new { status = status } };
        }

        [AuthorizeRole("Admin")]
        public ActionResult StaffList()
        {
            using (EquineDBEntities dc = new EquineDBEntities())
            {
                var emp = dc.Users.OrderBy(a => a.FirstName).ToList();
                return Json(new { data = emp }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult Save(int id)
        {
            using (EquineDBEntities dc = new EquineDBEntities())
            {
                var v = dc.Users.Where(a => a.UserId == id).FirstOrDefault();
                return View(v);
            }
        }

        [HttpPost]
        public ActionResult Save(User user)
        {
            bool status = false;
            if (ModelState.IsValid && IsEmailExist(user.EmaidId))
            {
                using (EquineDBEntities dc = new EquineDBEntities())
                {
                    if (user.UserId > 0)
                    {
                        var v = dc.Users.Where(a => a.UserId == user.UserId).FirstOrDefault();
                        if (v != null)
                        {
                            v.FirstName = user.FirstName;
                            v.LastName = user.LastName;
                            v.EmaidId = user.EmaidId;
                        }
                        else
                        {
                            //save
                            dc.Users.Add(user);
                        }

                        dc.SaveChanges();
                        status = true;
                    }
                }

            }
            return new JsonResult { Data = new { status = status } };
        }

        //validate existing email registered
        [NonAction]
        public bool IsEmailExist(string emailId)
        {
            using (EquineDBEntities dc = new EquineDBEntities())
            {
                var v = dc.Users.Where(a => a.EmaidId == emailId).FirstOrDefault();
                return v != null;
            }
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            using (EquineDBEntities dc = new EquineDBEntities())
            {
                var v = dc.Users.Where(a => a.UserId == id).FirstOrDefault();
                if (v != null)
                {
                    return View(v);
                }
                else
                {
                    return HttpNotFound();
                }


            }
        }

        [HttpPost]
        // [ActionName("Delete")]
        public ActionResult DeleteStaff(int id)
        {
            bool status = false;

            using (EquineDBEntities dc = new EquineDBEntities())
            {
                var v = dc.Users.Where(a => a.UserId == id).FirstOrDefault();
                if (v != null)
                {
                    dc.Users.Remove(v);
                    dc.SaveChanges();
                    status = true;
                }
            }
            return new JsonResult
            {
                Data = new { status = status }
            };
        }

        //new CRUD 
        public ActionResult ListStaff()
        {
            TempData["headline"] = "Staff Management";
            return View();
        }

        public JsonResult GetStaffList()
        {
            List<UserViewModel> userList = dc.Users.Where(x => x.Role == "Staff").Select(x => new UserViewModel
            {
                UserId = x.UserId,
                FirstName = x.FirstName,
                LastName = x.LastName,
                EmaidId = x.EmaidId,
                Role = x.Role

            }).ToList();

            return Json(userList, JsonRequestBehavior.AllowGet);
        }

        //autocomplete for staff selection
        public JsonResult GetStaffAutocomplete(string prefix)
        {
            List<UserViewModel> userList = dc.Users.Where(x => x.Role == "Staff" && x.FirstName.Contains(prefix)).Select(x => new UserViewModel
            {
                UserId = x.UserId,
                FirstName = x.FirstName,
                LastName = x.LastName
                //EmaidId = x.EmaidId,
                //Role = x.Role

            }).ToList();

            return Json(userList, JsonRequestBehavior.AllowGet);
        }

        //edit user
        //[HttpGet]
        public JsonResult GetStaffById(int userId)
        {
            User model = dc.Users.Where(x => x.UserId == userId).SingleOrDefault();
            string value = string.Empty;
            value = JsonConvert.SerializeObject(model, Formatting.Indented, new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });
            return Json(value, JsonRequestBehavior.AllowGet);
        }

        //approve booking using booking id
        public JsonResult ApproveBookingById(int bookingId)
        {
            var status=false;
            using(EquineDBEntities dc=new EquineDBEntities())
            {
                var booking = dc.Booking.SingleOrDefault(x => x.BookingId == bookingId);
                booking.Status = 1;//1 represnt approved booking
                dc.SaveChanges();
                status = true;

                return new JsonResult { Data = new { status=status}, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
        }

        //reject booking
        public JsonResult RejectBookingById(Comment comment)
        {
            var status = false;
            using (EquineDBEntities dc = new EquineDBEntities())
            {
                try
                {
                    //get userID

                    //change status to rejected
                    var booking = dc.Booking.SingleOrDefault(x => x.BookingId == comment.BookingId);
                    booking.Status = 2;//1 represent rejected booking
                    dc.SaveChanges();

                    //add comment to comment table
                    dc.Comment.Add(comment);
                    dc.SaveChanges();
                    status = true;
                }
                catch (Exception e)
                {
                    string error = e.ToString();
                }
                

                return new JsonResult { Data = new { status = status }, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }

        }

        public JsonResult SaveDataInDatabase(UserViewModel model)
        {
            var result = false;
            try
            {
                if (!IsEmailExist(model.EmaidId))
                {
                    if (model.UserId > 0)
                    {
                        //update existing staff
                        User user = dc.Users.SingleOrDefault(x => x.Status == "Active" && x.UserId == model.UserId);
                        user.FirstName = model.FirstName;
                        user.LastName = model.LastName;
                        user.EmaidId = model.EmaidId;
                        dc.SaveChanges();
                        result = true;
                    }
                    else
                    {
                        //for add new staff
                        User usr = new User();
                        usr.FirstName = model.FirstName;
                        usr.LastName = model.LastName;
                        usr.EmaidId = model.EmaidId;
                        //default password upon creation
                        usr.Password = Crypto.Hash("UTMEQUINE");
                        usr.Status = "Active";
                        usr.IsEmailVerified = 0;
                        usr.ActivationCode = Guid.NewGuid();
                        usr.Role = "Staff";
                        dc.Users.Add(usr);
                        dc.SaveChanges();
                        result = true;

                        //send email verification code
                        sendEmailVerification(usr.EmaidId, usr.ActivationCode.ToString());

                    }
                }

            }
            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting  
                        // the current instance as InnerException  
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        //send email notification
        public void sendEmailVerification(string emailId, string activationCode)
        {
            var verifyUrl = "/User/VerifyAccount/" + activationCode;
            var link = Request.Url.AbsoluteUri.Replace(Request.Url.PathAndQuery, verifyUrl);

            var fromEmail = new MailAddress("meorkhairulamir@gmail.com", " Equine");
            var toEmail = new MailAddress(emailId);
            var fromMailPassword = "keag2181";
            string subject = "Your account successfully created";

            string body = "<br></br> We are excited to tell you that your account is ready. Please click on link to verify your account " +
                "<a href='" + link + "'>" + link + "</a>";

            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromEmail.Address, fromMailPassword)


            };

            using (var message = new MailMessage(fromEmail, toEmail)
            {
                Subject = subject,
                Body = body,
                IsBodyHtml = true
            })

                smtp.Send(message);
        }


        //Verify email LINK using GET
        [HttpGet]
        public ActionResult VerifyAccount(string id)
        {
            //redirect to action from User Controllert,share functionality
            return RedirectToAction("VerifyAccount", "User", new { id = id });

        }

        //check event number per day
        public JsonResult CheckEventOnSpecificDay(string specDate)
        {
            using (EquineDBEntities dc = new EquineDBEntities())
            {
                var events = (from e in dc.Events
                          join u in dc.Users
                          on e.UserId equals u.UserId
                          where e.SpecDate == specDate
                          select new
                          {
                              Origin = "Event",
                              Writer = u.FirstName + " " + u.LastName,
                              Subject = e.Subject,
                              //Description = e.Description,
                              //Start = e.Start,
                              //End = e.End,
                              //ThemeColor = e.ThemeColor
                              // IsFullDay = e.IsFullDay

                          }).Union(from b in dc.Booking
                                   join u in dc.Users
                                   on b.UserId equals u.UserId
                                   join p in dc.Package
                                   on b.PackageId equals p.PackageId
                                   where b.SpecDate == specDate
                                   select new
                                   {
                                       Origin = "Booking",
                                       Writer = u.FirstName + " " + u.LastName,
                                       Subject = "Booking",
                                       //Description = p.PackageName + "X" + b.Quantity + b.Status,
                                       //Start = b.BookingDate,
                                       //End = b.BookingDate,
                                       //ThemeColor = "blue"
                                       ////IsFullDay = null;

                                   }).ToList();

            //var events = dc.Events.ToList();
            return new JsonResult { Data = events, JsonRequestBehavior = JsonRequestBehavior.AllowGet };

            }

        }
    }
}