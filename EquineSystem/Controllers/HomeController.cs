﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EquineSystem.Security;
using EquineSystem.Models;

namespace EquineSystem.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        [AuthorizeRole("Admin", "User","Staff")]
        public ActionResult Index()
        {
            TempData["headline"] = "Profile";
            return View();
        }

        [AuthorizeRole("Admin","User", "Staff")]
        public ActionResult Home()
        {
            return View();
        }

        public ActionResult Unauthorized()
        {
            return View();
        }

        
    }
}