﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EquineSystem.Models;




namespace EquineSystem.Controllers
{
    public class PaymentController : Controller
    {
        // GET: Payment
        public ActionResult Index()
        {
            TempData["headline"] = "Payment";
            return View();
        }

        //return list of booking for payemnt
        public JsonResult GetAllBookingForPayment()
        {
            using (EquineDBEntities dc = new EquineDBEntities())
            {
                var bookingListForPayment = (from b in dc.Booking
                                   join u in dc.Users
                                   on b.UserId equals u.UserId
                                   join p in dc.Package
                                   on b.PackageId equals p.PackageId
                                   where b.Status == 1 //select only booking which been approved
                                   orderby b.TimeStamp descending
                                   select new
                                   {
                                       BookingId = b.BookingId,
                                       Customer = u.FirstName + " " + u.LastName,
                                       PackageName = p.PackageName,
                                       Quantity = b.Quantity,
                                       Status = b.Status,
                                       BookingDate = b.BookingDate

                                   }).ToList();

                return new JsonResult { Data = bookingListForPayment, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
        }

        [HttpGet]
        public JsonResult GetBookingForPayment(int bookingId)
        {
            using (EquineDBEntities dc = new EquineDBEntities())
            {

                var bookingPayment = (from b in dc.Booking
                                          join u in dc.Users
                                          on b.UserId equals u.UserId
                                          join p in dc.Package
                                          on b.PackageId equals p.PackageId
                                          where b.BookingId == bookingId
                                          select new
                                          {
                                              Customer = u.FirstName + " " + u.LastName,
                                              PackageName = p.PackageName,
                                              Quantity = b.Quantity,
                                              Price =p.Price,
                                              Total =p.Price*b.Quantity,
                                              BookingDate = b.BookingDate.ToString()

                                          }).ToList();

                return new JsonResult { Data = bookingPayment, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
        }

        [HttpPost]
        public JsonResult ConfirmedPayment(Payment payment, string user)
        {
            var status = false;
            int userId = 0;
            using (EquineDBEntities dc = new EquineDBEntities())
            {
                try
                {
                    //get userID
                    var userInCharge = dc.Users.SingleOrDefault(x => x.EmaidId == user);
                    userId = userInCharge.UserId;

                    //assign user id to payment
                    payment.UserId = userId;
                    payment.Timestamp = DateTime.Now;

                    //change status to paid
                    var booking = dc.Booking.SingleOrDefault(x => x.BookingId == payment.BookingId);
                    booking.Status = 3;//3 represent paid booking
                    dc.SaveChanges();

                    //add payment transaction to table
                    dc.Payment.Add(payment);
                    dc.SaveChanges();

                    status = true;
                }
                catch (Exception e)
                {
                    string error = e.ToString();
                }


                return new JsonResult { Data = new { status = status }, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
        }

        //print payment by order
        public ActionResult PrintBookingReceipt(int id)
        {
            //return new Rotativa.ViewAsPdf("GeneratePDF", model) { FileName = "TestViewAsPdf.pdf" }
            
        }

    }
}