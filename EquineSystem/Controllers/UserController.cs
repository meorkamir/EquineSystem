﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EquineSystem.Models;
using System.Net.Mail;
using System.Net;
using System.Web.Security;

namespace EquineSystem.Controllers
{
    public class UserController : Controller
    {
        // GET: User
        public ActionResult Index()
        {
            return View();
        }

        //Registration Action
        
        public ActionResult Registration()
        {
            return View();
        }

        
        //Registration POST action
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Registration([Bind(Exclude ="IsEmailVerified,ActivationCode")] User user)
        {
            // for viewbag/view validation
            bool Status = false;
            string Message = "";

            //Model validation
            if (ModelState.IsValid)
            {
               // email already exist
                var isExist = IsEmailExist(user.EmaidId);
                if (isExist)
                {
                    ModelState.AddModelError("EmailExist", "Email already exist");
                    return View(user);
                }

               // activation code
                #region Generate Activation Code
                user.ActivationCode = Guid.NewGuid();
                #endregion

                #region Password Hashing
                user.Password = Crypto.Hash(user.Password);
                user.ConfirmPassword = Crypto.Hash(user.ConfirmPassword);
                #endregion
                user.IsEmailVerified = 0; //

                #region Save to DB
                using (EquineDBEntities dc = new EquineDBEntities())
                {
                    dc.Users.Add(user);
                    dc.SaveChanges();

                    //send email
                    SendVerificationEmail(user.EmaidId, user.ActivationCode.ToString());
                    Message = "Registration already done. Activation link sent to your email id" + user.EmaidId;
                    Status = true;

                }
                #endregion

                


            }
            else
            {
                Message = "Invalid request";
            }


            ViewBag.Message = Message;
            ViewBag.Status = Status;
            return View(user);
        }


        //Verify email LINK using GET
        [HttpGet]
        public ActionResult VerifyAccount(string id)
        {
            bool Status = false;
            using(EquineDBEntities dc=new EquineDBEntities())
            {
                dc.Configuration.ValidateOnSaveEnabled = false; //this line added to avoid confirm password
                                                                // does not match issue on save changes
                var v=dc.Users.Where(a => a.ActivationCode==new Guid(id)).FirstOrDefault();
                if(v != null)
                {
                    v.IsEmailVerified = 1;
                    dc.SaveChanges();
                    Status = true;

                }
                else
                {
                    ViewBag.Message = "Invalid Request";
                }
                ViewBag.Status = Status;
                return View();
                
            }
        }

        //Login
        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        //Login POST
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(UserLogin login, string ReturnUrl)
        {
            string message = "";

            using(EquineDBEntities dc=new EquineDBEntities())
            {
                var v = dc.Users.Where(a => a.EmaidId == login.EmailID).FirstOrDefault();
                if(v != null)
                {
                    if (string.Compare(Crypto.Hash(login.Password), v.Password) == 0)
                    {
                        int timeout = login.RememberMe ? 525600 : 20; //525600== 1 year
                        var ticket = new FormsAuthenticationTicket(login.EmailID, login.RememberMe, timeout);
                        string encrypted = FormsAuthentication.Encrypt(ticket);

                        var cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encrypted);
                        cookie.Expires = DateTime.Now.AddMinutes(timeout);
                        cookie.HttpOnly = true;
                        Response.Cookies.Add(cookie);

                       // string ReturnUrl = null;
                        if (Url.IsLocalUrl(ReturnUrl))
                        {
                            return Redirect(ReturnUrl);
                        }
                        else
                        {
                            //TempData["email"] = login.EmailID;
                            return RedirectToAction("Index", "Home");
                        }
                    }
                    else
                    {
                        message = "Invalid credential";
                    }
                }
                else
                {
                    message = "Invalid credential";
                }
            }

            ViewBag.Message = message;
            return View();

        }

        //Logout
        [Authorize]
        //[HttpPost]
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Login", "User");
        }


        [NonAction]
        public bool IsEmailExist(string emailId)
        {
            using(EquineDBEntities dc =new EquineDBEntities())
            {
                var v = dc.Users.Where(a => a.EmaidId == emailId).FirstOrDefault();
                return v != null;
            }
        }

        [NonAction]
        public void SendVerificationEmail(string emailId, string activationCode)
        {
            //var scheme = Request.Url.Scheme;
            //var host = Request.Url.Host;
            //var port = Request.Url.Port;

            //string url=scheme=""
            var verifyUrl = "/User/VerifyAccount/" + activationCode;
            var link = Request.Url.AbsoluteUri.Replace(Request.Url.PathAndQuery, verifyUrl);

            var fromEmail = new MailAddress("meorkhairulamir@gmail.com", " Equine");
            var toEmail = new MailAddress(emailId);
            var fromMailPassword = "keag2181";
            string subject = "Your account successfully created";

            string body = "<br></br> We are excited to tell you that your account is ready. Please click on link to verify your account "+
                "<a href='"+link+"'>"+link+"</a>";

            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials=false,
                Credentials = new NetworkCredential(fromEmail.Address, fromMailPassword)
            

            };

            using (var message = new MailMessage(fromEmail, toEmail)
            {
                Subject = subject,
                Body = body,
                IsBodyHtml = true
            })

                smtp.Send(message);
            


        }
    }

    
}