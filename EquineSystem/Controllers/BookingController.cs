﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EquineSystem.Models;
using Newtonsoft.Json;
using System.Globalization;


namespace EquineSystem.Controllers
{
    public class BookingController : Controller
    {
        //initialiaze context
        EquineDBEntities dc = new EquineDBEntities();

        //page for STAFF/ADMIN manage booking
        public ActionResult Index()
        {
            TempData["headline"] = "Reservation record/ Management";
            return View();
        }

        //page for STAFF/ADMIN manage booking
        public ActionResult ManageBooking()
        {
            TempData["headline"] = "Reservation record/ Management";
            return View();
        }

        //page for USER to view his/her booking record and status
        public ActionResult ListBooking()
        {
            
            TempData["headline"] = "Booking record";
            return View();
        }

        //List of all booking made by user
        [HttpGet]
        public JsonResult GetBookingListByEmail(string emailId)
        {
            using(EquineDBEntities dc=new EquineDBEntities())
            {
                var bookingList = (from b in dc.Booking
                                join u in dc.Users
                                on b.UserId equals u.UserId
                                join p in dc.Package
                                on b.PackageId equals p.PackageId
                                where u.EmaidId== emailId
                                   select new
                                {
                                    //Origin = "Event",
                                    PackageName=p.PackageName,
                                    Quantity=b.Quantity,
                                    Status =b.Status,
                                    BookingDate=b.BookingDate

                                }).ToList();

                return new JsonResult { Data = bookingList, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
        }

        //all booking under status pending
        [HttpGet]
        public JsonResult GetAllBooking()
        {
            using (EquineDBEntities dc = new EquineDBEntities())
            {
                var bookingList = (from b in dc.Booking
                                   join u in dc.Users
                                   on b.UserId equals u.UserId
                                   join p in dc.Package
                                   on b.PackageId equals p.PackageId
                                   where b.Status==0 orderby b.BookingDate ascending
                                   select new
                                   {
                                       BookingId=b.BookingId,
                                       Customer=u.FirstName+" "+u.LastName,
                                       PackageName = p.PackageName,
                                       Quantity = b.Quantity,
                                       Status = b.Status,
                                       TimeStamp = b.TimeStamp

                                   }).ToList();

                return new JsonResult { Data = bookingList, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
        }

        //get booking detail by booking id
        [HttpGet]
        public JsonResult GetBookingPending(int bookingId)
        {
            
            using (EquineDBEntities dc = new EquineDBEntities())
            {

                var bookingPendingList = (from b in dc.Booking
                                          join u in dc.Users
                                          on b.UserId equals u.UserId
                                          join p in dc.Package
                                          on b.PackageId equals p.PackageId
                                          where b.BookingId == bookingId
                                          select new
                                          {
                                              Customer = u.FirstName + " " + u.LastName,
                                              PackageName = p.PackageName,
                                              Quantity = b.Quantity,
                                              BookingDate = b.BookingDate.ToString()

                                          }).ToList();
               
                return new JsonResult { Data = bookingPendingList, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
        }

        public JsonResult CheckEventOnDay()
        {
            return new JsonResult { Data = new { message = "Yeayy" }, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public JsonResult EventByDate(int bookingId)
        {
            
            using (EquineDBEntities dc = new EquineDBEntities())
            {
                //select specDate from booking id
                var v = dc.Booking.Where(a => a.BookingId == bookingId).FirstOrDefault();
                var specDate = v.SpecDate;
                //make list of event on same date
                var events = (from e in dc.Events
                              join u in dc.Users
                              on e.UserId equals u.UserId
                              where e.SpecDate == specDate
                              select new
                              {
                                  Origin = "Event",
                                  Writer = u.FirstName + " " + u.LastName,
                                  Subject = e.Subject

                              }).Union(from b in dc.Booking
                                       join u in dc.Users
                                       on b.UserId equals u.UserId
                                       join p in dc.Package
                                       on b.PackageId equals p.PackageId
                                       where b.SpecDate == specDate && b.BookingId !=bookingId
                                       select new
                                       {
                                           Origin = "Booking",
                                           Writer = u.FirstName + " " + u.LastName,
                                           Subject = "Booking"

                                       }).ToList();

                return new JsonResult { Data = events, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
           // return new JsonResult { Data = new { bookingId = bookingId }, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        //statistic
        public JsonResult BookingStatistic()
        {
            using (EquineDBEntities dc = new EquineDBEntities())
            {
                var stat = (from b in dc.Booking
                            group b by b.Status into bGroup
                            select new
                            {
                                Status = bGroup.Key,
                                Count = bGroup.Count()
                            }).ToList();

                return new JsonResult { Data = stat, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }


        }
    }
}