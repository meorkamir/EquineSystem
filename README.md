## Equine Management System
Develop using : ASP MVC5,Entity framework, LINQ, Jquery, Ajax, HTML, Css

Functionality:

- Staff management

- Equine Package Management

- Reservation management

- Customize calender for event handling

- Statistic

## Screenshot

### Staff management
![staffman](/uploads/7a80fb80538413246c3a4e31bf835247/staffman.png)

### Customized calender
![calender](/uploads/0e312dad23988b09514fc86003030504/calender.png)

### Booking
![booking](/uploads/520cf9c2394bfe3189b20b7d1403584a/booking.png)

### Package management
![packageMan](/uploads/1b782df1f6990c5c7760a4d92ec5f626/packageMan.png)

###  Statistic
![stat](/uploads/24a01b5afeff58c0a951e42fd21aa977/stat.png)